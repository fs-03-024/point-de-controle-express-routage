const express = require('express');
const app = express();

// Servir les fichiers statiques dans le dossier "public"
app.use(express.static('public'));

// Définir les routes
const homeRouter = require('./routes/home');
const servicesRouter = require('./routes/services');
const contactRouter = require('./routes/contact');

app.use('/', homeRouter);
app.use('/services', servicesRouter);
app.use('/contact', contactRouter);

// Configurer EJS
app.set('view engine', 'ejs');
app.set('views', 'views');

// Démarrer le serveur
const port = 3000;
app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`);
});