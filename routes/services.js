const express = require('express');
const router = express.Router();

// Route pour la page "Nos services"
router.get('/', (req, res) => {
  res.send(`
    <h1>Nos services</h1>
    <ul>
      <li>Service 1</li>
      <li>Service 2</li>
      <li>Service 3</li>
    </ul>
    <nav>
      <a href="/">Accueil</a>
      <a href="/services">Nos services</a>
      <a href="/contact">Contactez-nous</a>
    </nav>
  `);
});

module.exports = router;