const express = require('express');
const router = express.Router();

// Route pour la page "Contactez-nous"
router.get('/', (req, res) => {
  res.send(`
    <h1>Contactez-nous</h1>
    <p>Vous pouvez nous joindre à l'adresse email suivante : contact@monsite.com</p>
    <nav>
      <a href="/">Accueil</a>
      <a href="/services">Nos services</a>
      <a href="/contact">Contactez-nous</a>
    </nav>
  `);
});

module.exports = router;