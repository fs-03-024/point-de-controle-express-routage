const express = require('express');
const router = express.Router();

// Route pour la page d'accueil
router.get('/', (req, res) => {
  res.send(`
    <!DOCTYPE html>
    <html>
      <head>
        <title>Accueil</title>
        <link rel="stylesheet" href="/css/style.css">
      </head>
      <body>
        <header>
    <h1>Bienvenue sur notre site web</h1>
    <p>Découvrez nos services et contactez-nous pour plus d'informations.</p>
    <nav>
      <a href="/">Accueil</a>
      <a href="/services">Nos services</a>
      <a href="/contact">Contactez-nous</a>
    </nav>
  `);
});

module.exports = router;