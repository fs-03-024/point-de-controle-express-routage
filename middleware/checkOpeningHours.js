module.exports = (req, res, next) => {
    const currentDate = new Date();
    const currentDay = currentDate.getDay();
    const currentHour = currentDate.getHours();
  
    // Vérifier si la requête est pendant les heures d'ouverture (du lundi au vendredi, de 9h à 17h)
    if (
      currentDay >= 1 &&
      currentDay <= 5 &&
      currentHour >= 9 &&
      currentHour < 17
    ) {
      next();
    } else {
      res.status(503).send('Le site web est fermé pour le moment.');
    }
  };